FROM docker:latest

RUN apk update
RUN apk add py-pip
RUN apk add bash
RUN pip install bash docker-compose

COPY templates /templates
COPY scripts /scripts
COPY AppCycles.sh /
RUN cp -r /templates /setup
RUN cp /templates/AppCycles-server/AppCycles.config.template /AppCycles.config
RUN source /AppCycles.config

CMD ./AppCycles.sh start
EXPOSE 8080
EXPOSE 50000
