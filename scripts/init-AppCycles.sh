#!/bin/bash

set -e

clear

function initAppCyclesScript {
    BG_RED='\033[0;41;93m'
    BG_GREEN='\033[0;31;42m'
    BG_BLUE='\033[0;44;93m'
    BLUE='\033[0;94m'
    YELLOW='\033[0;93m'
    NC='\033[0m' # No Color

    rm -rf temp 2> /dev/null | true
    mkdir -p temp
}

function initAppCyclesConfig {
    echo "#!/bin/bash" > temp/AppCycles.config
    echo -e "" >> temp/AppCycles.config
}

function inputVariable {

    TITLE=$1
    VARIABLE_NAME=$2
    VALUE=${!VARIABLE_NAME}
    echo -e -n "${TITLE}\n\t[${BLUE}${VALUE}${NC}]? "
    read -r
    if [[ "$REPLY" != "" ]]; then
        VALUE="$REPLY"
    fi
    echo "export $VARIABLE_NAME=${VALUE}" >> temp/AppCycles.config
    export $VARIABLE_NAME=${VALUE}
}

function inputTextVariable {

    TITLE=$1
    VARIABLE_NAME=$2
    VALUE=${!VARIABLE_NAME}
    echo -e -n "${TITLE}\n\t[${BLUE}${VALUE}${NC}]? "
    read -r
    if [[ "$REPLY" != "" ]]; then
        VALUE="$REPLY"
    fi
    echo "export $VARIABLE_NAME='${VALUE}'" >> temp/AppCycles.config
    export $VARIABLE_NAME='${VALUE}'
}

initAppCyclesScript
initAppCyclesConfig

inputVariable "AppCycles-master Docker image" APPCYCLES_MASTER_VERSION
inputVariable "AppCycles host IP address (set to * for automatic IP calculation)" APPCYCLES_HOST_IP
inputVariable "Jenkins server HTTP port" JENKINS_HTTP_PORT_FOR_SLAVES
inputVariable "Jenkins JNLP port for slaves" JENKINS_SLAVE_AGENT_PORT
inputTextVariable "AppCycles customization folder root path" APPCYCLES_CUSTOMIZATION_FOLDER
inputVariable "Number of executers on master" JENKINS_ENV_EXECUTERS
inputTextVariable "AppCycles banner title" APPCYCLES_MASTER_TITLE_TEXT
inputVariable "AppCycles banner title color" APPCYCLES_MASTER_TITLE_COLOR
inputVariable "AppCycles banner background color" APPCYCLES_MASTER_BANNER_COLOR

cp temp/AppCycles.config AppCycles.config


