# AppCycles-Jenkins-Server-Compose

AppCycles Jenkins customized server based on: docker-compose, https://github.com/odavid/my-bloody-jenkins and https://github.com/TikalCI/AppCycles-jenkins-compose

With this repository, you can establish **AppCycles-Jenkins-compose** server - for loading a well-established Jenkins server.

In order to establish a  **AppCycles-Jenkins-compos**, follow the below instructions on the server you want to host it:

1. Make sure you have the following commands installed: **git**, **docker** & **docker-compose**.
1. clone this repository (https://gitlab.com/tikalk/appcycles/appcycles-jenkins/AppCycles-Jenkins-composed) to a local folder and cd to it.
1. Run _**./AppCycles.sh init**_ to see that the path to the SSH private key file is correct and to set all values.
1. Run _**./AppCycles.sh start**_ to load the server. 
1. The first load will take at least 10 minutes, so please wait until the prompt is back.
1. Once the load is over, browse to [http://localhost:8080](http://localhost:8080) and login with admin/admin credentials.

Once the server is up, you can modify it (e.g. add LDAP configuration, add seed jobs, add credentials and much more) following the instructions as in [AppCycles-bloody-jenkins](https://github.com/TikalCI/AppCycles-bloody-jenkins) and all files in the 'customization' folder.

The _**./AppCycles.sh**_ script have the following actions: **start**, **stop**, **restart**, **info**, **init**, **upgrade**, **status**, **apply**, **version**.

The loaded server is already configured to work with [AppCycles-Jenkins-Pipeline](https://gitlab.com/tikalk/appcycles/appcycles-jenkins/appcycles-jenkins-pipeline) so you can start working with it.
